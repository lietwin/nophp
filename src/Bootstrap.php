<?php
  declare(strict_types = 1);
  
  namespace PhpBoot;
  
  require __DIR__ . '/../vendor/autoload.php';

  use \Whoops\Run;
  use \Whoops\Handler\PrettyPageHandler;

  use \FastRoute\Dispatcher;
  use \FastRoute\RouteCollector;

  error_reporting(E_ALL);

  $env = 'dev';

  $injector = include('Dependencies.php');

  /* setting up the error handler */
  $whoops = new Run;
if ($env !== 'prod') {
    $whoops->pushHandler(new PrettyPageHandler);
} else {
    $whoops->pushHandler(function ($e) {
        echo 'TODO: Friendly error page and send an email to the developper';
    });
}

  $whoops->register();
  

/* setting up request and response objects*/
$request = $injector->make('Http\HttpRequest');
$response = $injector->make('Http\HttpResponse');


foreach ($response->getHeaders() as $header) {
    header($header, false);
}

/* setting the template engine*/
$renderer = $injector->make('PhpBoot\Template\MustacheRenderer');


/*setting up routing dispatcher */
$routeDefinitionCallback = function (RouteCollector $r) {
    $routes = include('Routes.php');
    $baseUrl = '/public';
    foreach ($routes as $route) {
        $r->addRoute($route[0], $baseUrl . $route[1], $route[2]);
    }
};

$dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

$routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getPath());

switch ($routeInfo[0]) {
    case Dispatcher::NOT_FOUND:
        $response->setContent('404 - Page not found');
        $response->setStatusCode(404);
        break;
    case Dispatcher::METHOD_NOT_ALLOWED:
        $response->setContent('405 - Method not allowed');
        $response->setStatusCode(405);
        break;
    case Dispatcher::FOUND:
        $className = $routeInfo[1][0];
        $method = $routeInfo[1][1];
        $vars = $routeInfo[2];

        $class = $injector->make($className);
        $class->$method($vars);

        break;
}

echo $response->getContent();
