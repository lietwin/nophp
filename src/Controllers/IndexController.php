<?php
  declare(strict_types = 1);
  
  namespace PhpBoot\Controllers;

  use Http\Request;
  use Http\Response;
  use PhpBoot\Template\Renderer;

  class IndexController {
      private $response;
      private $request;
      private $render;

      public function __construct(Request $request, Response $response, Renderer $renderer){
          $this->request = $request;
          $this->response = $response;
          $this->renderer = $renderer;
      }

      public function indexAction() {
          $data = ['name' => $this->request->getParameter('name', 'Stranger')];
          
          $html = $this->renderer->render('index', $data);
         $this->response->setContent($html);
         
      }
  }