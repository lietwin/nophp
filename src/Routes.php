<?php
    declare(strict_types = 1);
    namespace PhpBoot;



    return [
        ['GET', '/', ['PhpBoot\Controllers\IndexController', 'indexAction']],

        ['GET', '/hello-world', function () {
            echo 'Hello World';
        }],

        ['GET', '/another-route', function () {
            echo 'This works too';
        }],
];
