<?php
  declare(strict_types = 1);

  namespace PhpBoot\Template;

  interface Renderer{
      public function render($template, $data= []) : string;
  }